# SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
# SPDX-License-Identifier: BSD-3-Clause

# -*- coding: utf-8 -*-
import os
import string
import random

from authlib.integrations.requests_client import OAuth2Session
from authlib.oauth2.rfc7523 import PrivateKeyJWT, private_key_jwt_sign
import requests
import urllib3
import yaml
import sys
import argparse
import logging

logging.basicConfig(level=logging.DEBUG)
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

# arg parser
parser = argparse.ArgumentParser()
parser.add_argument('-c', '--config',
                    help='path to YAML file with PUSH project configuration (push-server.yml by default)',
                    default="push-server.yml",
                    dest="config")
parser.add_argument('-r', '--regID',
                    help='registration ID to send push message (e3bbd700-6a76-4ad6-a699-ac284da5b1ab by default)',
                    default="e3bbd700-6a76-4ad6-a699-ac284da5b1ab",
                    dest="registrationID")
parser.add_argument('-m', '--messageText',
                    help='message text for push notification (some text by default)',
                    default="some message",
                    dest="messageText")
parser.add_argument('-a', '--action',
                    help='one of "send" (send message - used by default), "get-project" (get project details), '
                         '"update-key" (generate new pair and update public key)',
                    default='send',
                    dest='action')
if len(sys.argv) == 1:
    parser.print_help()
    exit(1)

args = parser.parse_args()
print(args)

############# PARSE YAML FILE FROM PUSH SERVER IF EXIST ##################
with open(args.config, 'r') as stream:
    try:
        options = yaml.safe_load(stream)
        options = options.get("push_notification_system")
        push_public_address = options.get('push_public_address')  # "https://someserver.ru/push/public"
        project_id = options.get('project_id')  # "pervyi_btr3bcln0kjd77bpnrk0"
        client_id = options.get('client_id')  # "pervyi_btr3bcln0kjd77bpnrk0"
        private_key_id = options.get('key_id')  # "public:cUbIeXoCnB"
        private_key = options.get('private_key').encode('ascii')
        audience = options.get('audience').replace(',',
                                                   ' ')  # comma must be replaced by space to use audience in 'openid connect' client
        scope = options.get('scopes')
        api_url = options.get('api_url')
        token_endpoint_address = options.get('token_url')
        config_loaded = True
        print("Config loaded from push-server.yml\n")
    except yaml.YAMLError as exc:
        print(exc)

############# PARSE RAW 'CONFIG' FILE FROM PUSH SERVER ##################
if not config_loaded:
    myvars = {}
    pem = ''
    pem_started = False
    with open("config") as myfile:
        for line in myfile:
            name, var = line.partition(":")[::2]
            if name in ('Адрес пуш сервера', 'scope', 'audience', 'clientId', 'privateKeyId', 'ID проекта'):
                myvars[name.strip()] = var.strip()

            if line.find('BEGIN RSA PRIVATE KEY') > 0:
                pem += var.lstrip()
                pem_started = True
            elif line.find('END RSA PRIVATE KEY') > 0:
                pem += line
                pem_started = False
            elif pem_started:
                pem += line

    push_public_address = myvars['Адрес пуш сервера']  # "https://someserver.ru/push/public"
    api_url = push_public_address + '/push/public/api/v1/'
    project_id = myvars['ID проекта']  # "pervyi_btr3bcln0kjd77bpnrk0"
    client_id = myvars['ID проекта']  # "pervyi_btr3bcln0kjd77bpnrk0"
    private_key_id = myvars['privateKeyId']  # "public:cUbIeXoCnB"
    private_key = pem.encode('ascii')
    audience = myvars['audience'].replace(',',
                                          ' ')  # comma must be replaced by space to use audience in 'openid connect' client
    scope = myvars['scope']
    print("Config loaded from plain `config` file\n")

print('Project options:')
print(f'\tpush_public_address: "{push_public_address}"')
print(f'\tapi_url: "{api_url}"')
print(f'\tproject_id: "{project_id}"')
print(f'\tclient_id: "{client_id}"')
print(f'\taudience: "{audience}"')
print(f'\tscope: "{scope}"')
print(f'\tprivate_key_id: "{private_key_id}"')
print(f'\tprivate_key: "{private_key}"')

############# GET TOKEN_ENDPOINT FROM .WELL-KNOWN ##################
if token_endpoint_address == "":
    well_known_address = api_url + ".well-known"
    result = requests.get(well_known_address, verify=False).json()
    token_endpoint_address = result['token_endpoint']
    print(f'Got token_endpoint:')

print(f'\ttoken_url: "{token_endpoint_address}"')


############# FETCH ACCESS_TOKEN ##################
class NewAuthMethod(PrivateKeyJWT):
    name = 'private_key_jwt'

    def __init__(self, token_endpoint=None, claims=None, header=None):
        super().__init__(token_endpoint, claims)
        self.header = header

    def sign(self, auth, token_endpoint):
        return private_key_jwt_sign(
            auth.client_secret,
            client_id=auth.client_id,
            token_endpoint=token_endpoint,
            claims=self.claims,
            header=self.header
        )


def getToken(private_key_id, private_key):
    header = {
        'kid': private_key_id,
    }
    print(f'Trying to get token with key {header}')

    session = OAuth2Session(
        client_id, private_key,
        scope=scope,
        token_endpoint_auth_method='private_key_jwt',
    )
    session.register_client_auth_method(NewAuthMethod(
        token_endpoint=token_endpoint_address,
        header=header,
    ))
    r = session.fetch_token(
        url=token_endpoint_address,
        audience=audience,
        verify=False,
    )
    access_token = r['access_token']
    print(f'Result:')
    print(f'\tresponse: {r}')
    print(f'\taccess_token: {access_token}')
    return access_token


access_token = getToken(private_key_id, private_key)

############# GET PROJECT DETAILS ##################
if args.action == 'get-project':
    # wrong_project_id = "acs_1384_test43_ck07aoirvkjfbdk2hhvg"
    get_project_endpoint = api_url + "projects/" + project_id
    header = {
        "Authorization": "Bearer " + access_token,
        "Cache-Control": "no-cache"
    }

    response = requests.get(get_project_endpoint, "", verify=False, headers=header)
    print(f'Get project details result:')
    print(f'\tx-request-id: {response.headers["X-Request-Id"]}')
    print(f'\tcode: {response.status_code}')
    if response.status_code != 404:
        result = response.json()
        print(f'\tresponse: "{result}"')
    exit(0)


############# SEND EXAMPLE PUSH MESSAGE ##################

def send_push(regID, messageText):
    send_endpoint = api_url + "projects/" + project_id + "/messages"

    # registrationId received from mobile application after registration on push daemon
    registrationId = regID

    body = {
        "target": registrationId,
        "ttl": "2h",
        "type": "device",
        "notification": {
            "title": "some title",
            "message": messageText,
            "data": {
                "action": "command",
                "another_key": "value"
            }
        }
    }

    header = {
        "Authorization": "Bearer " + access_token
    }

    response = requests.post(send_endpoint, json=body, verify=False, headers=header)
    print(f'Send push message result:')
    print(f'\tcode: {response.status_code}')
    # print(f'\tresponse: "{response.content}"')
    if response.status_code != 404:
        result = response.json()
        print(f'\tresponse: "{result}"')

        if response.status_code == 202:
            message_id = result['id']
            print(f'\tid: "{message_id}"')

    return response.status_code


if args.action == 'send':
    send_push(args.registrationID, args.messageText)
    exit(0)

############# UPDATE KEY ##################
if args.action == 'update-key':
    keyPair_endpoint = api_url + "keyPairs"
    kid = ''.join(random.choices(string.ascii_letters +
                                 string.digits, k=10))
    body = {"alg": "RS256", "kid": kid, "use": "sig"}

    header = {
        "Authorization": "Bearer " + access_token
    }

    response = requests.post(keyPair_endpoint, json=body, verify=False, headers=header)
    print(f'Generate key pair result:')
    print(f'\tcode: {response.status_code}')
    # print(f'\tresponse: "{response.content}"')
    if response.status_code == 200:
        result = response.json()
        print(f'\tresponse: "{result}"')

        # store pem to update YAML later
        pem = result['private']['pem']
        new_kid = result['public']['jwk']['kid']

        # apply key
        updatePublicKey_endpoint = api_url + "projects/" + project_id + "/serviceAccounts/" + client_id + "/publicKeys"
        body = {
            "keys": [
                result['public']['jwk']
            ]
        }
        header = {
            "Authorization": "Bearer " + access_token
        }

        print(f'Update public key with new: {body}')
        response = requests.put(updatePublicKey_endpoint, json=body, verify=False, headers=header)
        print(f'Update public key result:')
        print(f'\tcode: {response.status_code}')

        if response.status_code == 200:
            # update yaml file
            new_file = {'push_notification_system':
                             {'project_id': project_id,
                              'push_public_address': push_public_address,
                              'api_url': api_url,
                              'client_id': client_id,
                              'scopes': scope,
                              'audience': audience,
                              'token_url': token_endpoint_address,
                              'key_id': new_kid,
                              'private_key': pem
                              }
                        }

            os.rename(args.config, args.config + '_old')

            with open(args.config, 'w') as file:
                documents = yaml.dump(new_file, file)

            # send push with OLD key
            if args.registrationID != "":
                print(f'Trying to send push with OLD token to regID {args.registrationID}...')
                send_push(args.registrationID, args.messageText)

            # send push with new key
            access_token = getToken(new_kid, pem)
            if args.registrationID != "":
                print(f'Trying to send push with new token to regID {args.registrationID}...')
                send_push(args.registrationID, args.messageText)

    exit(0)
