# Push Sender Python

A python utility that provides the ability to interact with a Push server.

## Usage
1. Set the configuration in one of two ways:
  * Priority - download the server YAML file from the project settings via the Push server console and save it next to main.py under the name push-server.yml.
  * If the push-server.yml file is not found, then all the information "as is" obtained from the project settings from the Push server functional administrator is inserted into the config file.
2. Install dependencies
```
python3 -m venv venv
source ./venv/bin/activate
pip install -r requirements.txt

```
3. Launch
```shell
python ./main.py \
      --action one of:
            * send - send message (default)
            * get-project - get information about the project
            * update-key - generate a new key pair, update the public key for the service account and send a test message (if the --regID parameter is specified)
      --config <path to app_server_xxx.yaml, downloaded from the Push project settings (push-server.yml by default)>
      --regID <registration ID received in the push daemon or emulator during registration (default e3bbd700-6a76-4ad6-a699-ac284da5b1ab for TARGET NOT FOUND check)>
      --messageText <message for push notification body ("some text" by default)>
```

For example, to send a message you need to run the following command:
```shell
 python main.py --action send --config ~/configs/push/push-feat/app_server_kaa_smoke_111_c5kq2iiq44trvsk5lsm0.yaml --regID 22be010d-a5cc-4f9d-96be-366d4c5432ca
```

After successful sending, the program will display something like this:
```shell
Project options:
    push_public_address: "http://ocs-push-dev.devel.pro:8009/push/public"
    project_id: "novyi_proekt_btpivpvl5abfanlsq8r0"
    client_id: "novyi_proekt_btpivpvl5abfanlsq8r0"
    audience: "ocs-auth-public-api-gw ocs-push-public-api-gw"
    scope: "openid offline message:update"
    private_key_id: "public:sVnj2ES7UQ"
    private_key: "b'-----BEGIN RSA PRIVATE KEY-----
                    \nMIIJ...RYJhsSqPoLA==\n-----END RSA PRIVATE KEY-----\n'"
Got token_endpoint:
    "http://ocs-push-dev.ompcloud.ru/auth/public/oauth2/token"
Result:
    response: {'access_token': 'eyJhbGci...fojLY', 'expires_in': 3599, 'scope': 'openid offline message:update', 'token_type': 'bearer', 'expires_at': 1601966682}
    access_token: eyJhbGci...fojLY
Send push message result:
    response: "{'expiredAt': '2020-10-06T07:44:43.967576Z', 'id': '1526c09c-1ca4-48ea-8357-e1b8aa2f3fc3', 'notification': {'data': {'action': 'command', 'another_key': 'value'}, 'message': 'somem    message', 'title': 'some title'}, 'status': '', 'string': '', 'target': '7cefc0d1-b262-4bcc-a959-497c7bfd38f4', , 'type': 'device'}"
    id: "1526c09c-1ca4-48ea-8357-e1b8aa2f3fc3"
```

To get information about the project the command would be like this:
```shell
python main.py --action get-project --config ~/configs/push/push-feat/app_server_kaa_smoke_111_c5kq2iiq44trvsk5lsm0.yaml
```

To reissue keys for your service account, the command will look like this:
```shell
python main.py --action update-key --config ~/configs/push/push-feat/app_server_kaa_smoke_111_c5kq2iiq44trvsk5lsm0.yaml
```

If you want to immediately check that the message is sent successfully with the new key, you need to specify Registration ID as a parameter:
```shell
python main.py --action update-key --config ~/configs/push/push-feat/app_server_kaa_smoke_111_c5kq2iiq44trvsk5lsm0.yaml --regID 22be010d-a5cc-4f9d-96be-366d4c5432ca
```

## Terms of Use and Participation

The source code of the project is provided under [the license](LICENSE.BSD-3-Clause.md),
which allows its use in third-party applications.

The [contributor agreement](CONTRIBUTING.md) documents the rights granted by contributors
of the Open Mobile Platform.

Information about the contributors is specified in the [AUTHORS](AUTHORS.md) file.

[Code of conduct](CODE_OF_CONDUCT.md) is a current set of rules of the Open Mobile
Platform which informs you how we expect the members of the community will interact
while contributing and communicating.

## Project Structure

* **[main.py](main.py)** file defining the python script that implements the utility.
* **[push-server.yml](push-server.yml)** configuration file of the push project.
* **[config](config)** file to manually perform the push client configuration.
* **[request.http](request.http)** file containing sample requests for sending push messages.

## Dependencies

+ authlib
+ urllib3
+ requests
+ pyyaml

## This document in Russian / Перевод этого документа на русский язык

- [README.ru.md](README.ru.md)
