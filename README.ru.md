# Push Sender Python

Утилита на python, которая предоставляет возможность взаимодействия с Push-сервером.

## Использование
1. Задать конфигурацию одним из двух способов:
  * Приоритетный -- выгрузить серверный YAML-файл из настроек проекта через консоль Push-сервера и сохранить его рядом с main.py под именем push-server.yml
  * Если файла push-server.yml не будет найдено, то в файл config вставить всю информацию "как есть", полученную из настроек проекта от функционального администратора Push-сервера
2. Установить зависимости
```
python3 -m venv venv
source ./venv/bin/activate
pip install -r requirements.txt

```
3. Запустить
```shell
python ./main.py \
      --action one of:
            * send - отправить сообщение (используется по умолчанию)
            * get-project - получить сведения о проекте
            * update-key - сгенерировать новую пару ключей, обновить пуличный ключ для service account и отправить тестовое сообщение (в случае если параметр --regID задан) 
      --config <путь к app_server_xxx.yaml, выгруженному из настроек Push-проекта (по умолчанию push-server.yml)>
      --regID <registration ID, полученный в push-демоне или эмуляторе при регистрации (по умолчанию e3bbd700-6a76-4ad6-a699-ac284da5b1ab для проверки TARGET NOT FOUND)>
      --messageText <сообщение для тела push-уведомления ("some text" по умолчанию)>
```
Например, для отправки сообщения нужно выполнить такую команду:
```shell
 python main.py --action send --config ~/configs/push/push-feat/app_server_kaa_smoke_111_c5kq2iiq44trvsk5lsm0.yaml --regID 22be010d-a5cc-4f9d-96be-366d4c5432ca
```
После успешной отправки программа выведет примерно такой текст:
```shell
Project options:
    push_public_address: "http://ocs-push-dev.devel.pro:8009/push/public"
    project_id: "novyi_proekt_btpivpvl5abfanlsq8r0"
    client_id: "novyi_proekt_btpivpvl5abfanlsq8r0"
    audience: "ocs-auth-public-api-gw ocs-push-public-api-gw"
    scope: "openid offline message:update"
    private_key_id: "public:sVnj2ES7UQ"
    private_key: "b'-----BEGIN RSA PRIVATE KEY-----
                    \nMIIJ...RYJhsSqPoLA==\n-----END RSA PRIVATE KEY-----\n'"
Got token_endpoint:
    "http://ocs-push-dev.ompcloud.ru/auth/public/oauth2/token"
Result:
    response: {'access_token': 'eyJhbGci...fojLY', 'expires_in': 3599, 'scope': 'openid offline message:update', 'token_type': 'bearer', 'expires_at': 1601966682}
    access_token: eyJhbGci...fojLY
Send push message result:
    response: "{'expiredAt': '2020-10-06T07:44:43.967576Z', 'id': '1526c09c-1ca4-48ea-8357-e1b8aa2f3fc3', 'notification': {'data': {'action': 'command', 'another_key': 'value'}, 'message': 'somem    message', 'title': 'some title'}, 'status': '', 'string': '', 'target': '7cefc0d1-b262-4bcc-a959-497c7bfd38f4', , 'type': 'device'}"
    id: "1526c09c-1ca4-48ea-8357-e1b8aa2f3fc3"
```

Для получения информации о проекте команда будет такой:
```shell
python main.py --action get-project --config ~/configs/push/push-feat/app_server_kaa_smoke_111_c5kq2iiq44trvsk5lsm0.yaml
```

Для перевыпуска ключей для своего service account команда будет выглядеть так:
```shell
python main.py --action update-key --config ~/configs/push/push-feat/app_server_kaa_smoke_111_c5kq2iiq44trvsk5lsm0.yaml
```
Если захочется сразу же проверить, что сообщение успешно отправляется с новым ключом, то нужно указать Registration ID в качестве параметра:
```shell
python main.py --action update-key --config ~/configs/push/push-feat/app_server_kaa_smoke_111_c5kq2iiq44trvsk5lsm0.yaml --regID 22be010d-a5cc-4f9d-96be-366d4c5432ca
```

## Условия использования и участия

Исходный код проекта предоставляется по [лицензии](LICENSE.BSD-3-Clause.md),
которая позволяет использовать его в сторонних приложениях.

[Соглашение участника](CONTRIBUTING.md) регламентирует права,
предоставляемые участниками компании «Открытая Мобильная Платформа».

Информация об участниках указана в файле [AUTHORS](AUTHORS.md).

[Кодекс поведения](CODE_OF_CONDUCT.md) — это действующий набор правил
компании «Открытая Мобильная Платформа»,
который информирует об ожиданиях по взаимодействию между членами сообщества при общении и работе над проектами.

## Структура проекта

* Файл **[main.py](main.py)** определяет python-скрипт, который реализует утилиту.
* Файл **[push-server.yml](push-server.yml)** определяет конфигурацию push-проекта.
* Файл **[config](config)** позволяет вручную выполнить конфигурацию push-клиента.
* Файл **[request.http](request.http)** содержит примеры запросов для отправки push-сообщений.

## Зависимости

+ authlib
+ urllib3
+ requests
+ pyyaml

## This document in English

- [README.md](README.md)
